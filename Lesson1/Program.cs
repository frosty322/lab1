﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson1
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Console.WriteLine("Здравствуйте! Вас приетствует мульти-записная книжка на 1 сеанс работы программы!=)");
            List<Note> Memory = new List<Note>();
            int breaker = 10;
            while (breaker > 5) {
                Console.WriteLine("Что вы хотите сделать? Введите цифру от 1 до 4, чтобы выбрать:\n1 - добавить\n2 - редактировать\n3 - удалить\n4 - просмотреть все контакты\n0 - выйти из программы");
                string choise = Console.ReadLine();
                if (choise == "0"){
                    breaker = 4;
                    Console.Clear();
                    Console.WriteLine("До встречи!");
                    Console.WriteLine("\n");
                }else 
                    if (choise == "1")
                {
                    Console.WriteLine("\n");
                    AddMan(ref Memory);
                }else
                    if(choise == "4")
                {
                    Console.WriteLine("\n");
                    Console.Clear();
                    foreach (Note men in Memory)
                    {
                        Console.WriteLine(men.ToString());
                    }
                    Console.WriteLine("Других контактов нет!");
                }else
                    if (choise == "2")
                {
                    Console.Clear();
                    Console.WriteLine("Выберете какой контакт хотите изменить или нажмите 0 для выхода");
                    for(int i=0; i < Memory.Count; i++)
                    {
                        
                        Console.Write((i+1) + ") ");
                        Console.WriteLine(Memory[i]);
                    }
                    int m;
                    bool test= int.TryParse(Console.ReadLine(), out m);
                    while(m > Memory.Count|| m < 0 || test==false)
                    {
                        Console.WriteLine("Вы ввели что-то не то, попробуйте еще раз");
                        test = int.TryParse(Console.ReadLine(), out m);
                    }
                    if (m == 0)
                    {
                        breaker = 8;
                        Console.Clear();
                    }else
                    {
                        ChangeMan(ref Memory, m);
                    }
                }else
                    if(choise == "3")
                {
                    Console.Clear();
                    Console.WriteLine("Выберете какой контакт хотите удалить или нажмите 0 для выхода");
                    for (int i = 0; i < Memory.Count; i++)
                    {
                        Console.Write((i + 1) + ") ");
                        Console.WriteLine(Memory[i]);
                    }
                    int DeleteId;
                    bool test2 = int.TryParse(Console.ReadLine(), out DeleteId);
                    while(test2 == false)
                    {
                        Console.WriteLine("Введено недопутимое значение, возможно вы ошиблись, попробуйте еще раз");
                        test2 = int.TryParse(Console.ReadLine(), out DeleteId);
                    }
                    if (DeleteId == 0)
                    {
                        breaker = 8;
                    }else
                    DeleteMan(ref Memory, DeleteId);
                    Console.Clear();
                    
                }
                else
                {
                    Console.Clear();
                    breaker = 10;
                    Console.WriteLine("Введено недопустимое значение, попробуйте еще раз");
                }
                   
            }
        }
        public static void DeleteMan(ref List<Note> Memory, int m)
        {
            bool test = true;
            while(m > Memory.Count || m<0 || test == false)
            {
                Console.WriteLine("Введено недопустимое значение или контакт отсутствует");
                test = int.TryParse(Console.ReadLine(), out m);
            }
            if (m == 0)
            {
                return;
            }
            else
            {
                Memory.RemoveAt(m - 1);
                Console.WriteLine("Контакт удален");
            }
        }

        public static void ChangeMan(ref List<Note> Memory, int m)
        {
            Console.Clear();
            int breaker = 9;
            while (breaker > 6)
            {
                if (m == 0)
                {
                    breaker = 5;
                }
                else
                {
                    Console.WriteLine("Что вы хотите изменить?\n" + "1) Имя: " + Memory[m - 1].Name + "\n" +
                        "2) Фамилия: " + Memory[m - 1].SecondName + "\n" +
                        "3) Отчество: " + Memory[m - 1].FatherName + "\n" +
                        "4) Номер телефона: " + Memory[m - 1].PhoneNumber + "\n" +
                        "5) Страна: " + Memory[m - 1].Contry + "\n" +
                        "6) Дата рождения: " + Memory[m - 1].ResultDate + "\n" +
                        "7) Организация: " + Memory[m - 1].Organisation + "\n" +
                        "8) Должность: " + Memory[m - 1].Position + "\n" +
                        "9) Заметка: " + Memory[m - 1].SomethingElse + "\n" + "0) выйти в главное меню");
                    string ask = Console.ReadLine();
                    switch (ask)
                    {
                        case "0":
                            breaker = 5;
                            Console.Clear();
                            break;
                        case "1":
                            Console.Clear();
                            Console.WriteLine("Введите новое имя:");
                            Memory[m - 1].Name = Console.ReadLine();
                            Console.Clear();
                            break;
                        case "2":
                            Console.Clear();
                            Console.WriteLine("Введите новую фамилию:");
                            Memory[m - 1].SecondName = Console.ReadLine();
                            Console.Clear();
                            break;
                        case "3":
                            Console.Clear();
                            Console.WriteLine("Введите новое отчество:");
                            Memory[m - 1].FatherName = Console.ReadLine();
                            Console.Clear();
                            break;
                        case "4":
                            Console.Clear();
                            Console.WriteLine("Введите новый номер телефона:");
                            string n = Console.ReadLine();
                            long res;
                            while (false == long.TryParse(n, out res))
                            {
                                Console.WriteLine("Поле пустое или введено неверно, попробуйте еще раз:\n");
                                n = Console.ReadLine();
                            }
                            Memory[m - 1].PhoneNumber = res;
                            Console.Clear();
                            break;
                        case "5":
                            Console.Clear();
                            Console.WriteLine("Введите новую страну:");
                            Memory[m - 1].Contry = Console.ReadLine();
                            Console.Clear();
                            break;
                        case "6":
                            Console.Clear();
                            Console.WriteLine("Введите новую дату рождения:\nВведите день");
                            string day = Console.ReadLine();
                            int dres;
                            if (day == "")
                            {
                                Memory[m-1].DayOfBirdth = null;
                            }
                            else
                            {
                                while (false == int.TryParse(day, out dres) || int.Parse(day) < 0 || int.Parse(day) > 31)
                                {
                                    Console.WriteLine("Введите двузначное число от 1 до 31:\n");
                                    day = Console.ReadLine();
                                }
                                Memory[m - 1].DayOfBirdth = Convert.ToString(dres);
                            }

                            Console.WriteLine("Введите месяц рождения:");
                            string month = Console.ReadLine();
                            int mres;
                            if (day == "")
                            {
                                Memory[m - 1].MounthOfBirdth = null;
                            }
                            else
                            {
                                while (false == int.TryParse(month, out mres) || int.Parse(month) < 0 || int.Parse(month) > 12)
                                {
                                    Console.WriteLine("Введите двузначное число от 1 до 12:\n");
                                    month = Console.ReadLine();
                                }
                                Memory[m - 1].MounthOfBirdth = Convert.ToString(mres);
                            }
                            Console.WriteLine("Введите год рождения:");
                            string year = Console.ReadLine();
                            int yres;
                            if (year == "")
                            {
                                Memory[m - 1].YearOfBirdth = null;
                            }
                            else
                            {
                                while (false == int.TryParse(year, out yres) || int.Parse(year) < 0 || int.Parse(year) > 2019)
                                {
                                    Console.WriteLine("Введите правильный год:\n");
                                    year = Console.ReadLine();
                                }
                                Memory[m - 1].YearOfBirdth = Convert.ToString(yres);
                            }
                            Memory[m - 1].DateOfBirdth = new DateTime(int.Parse(Memory[m-1].YearOfBirdth), int.Parse(Memory[m - 1].MounthOfBirdth), int.Parse(Memory[m - 1].DayOfBirdth));
                            Memory[m - 1].ResultDate = Memory[m - 1].DateOfBirdth.ToShortDateString();
                            Console.Clear();
                            break;
                        case "7":
                            Console.Clear();
                            Console.WriteLine("Введите новую организацию:");
                            Memory[m - 1].Organisation = Console.ReadLine();
                            Console.Clear();
                            break;
                        case "8":
                            Console.Clear();
                            Console.WriteLine("Введите новую должность:");
                            Memory[m - 1].Position = Console.ReadLine();
                            Console.Clear();
                            break;
                        case "9":
                            Console.Clear();
                            Console.WriteLine("Введите новые заметки:");
                            Memory[m - 1].SomethingElse = Console.ReadLine();
                            Console.Clear();
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Введено недопустимое значение, попробуйте еще раз");
                            break;
                    }
                    
                }
            }
        }


        public static void AddMan(ref List<Note> Memory)
        {
            Console.Clear();
            Console.WriteLine("Введите имя:");
            Note man = new Note();
            string nameTest = Console.ReadLine().Trim();
            while(nameTest == "")
            {
                Console.WriteLine("Это обязательное поле!");
                nameTest = Console.ReadLine();
            }
            man.Name = nameTest;
            Console.WriteLine("Введите фамилию:");
            string sNameTest = Console.ReadLine().Trim();
            while (sNameTest == "")
            {
                Console.WriteLine("Это обязательное поле!");
                sNameTest = Console.ReadLine();
            }
            man.SecondName = sNameTest;
            Console.WriteLine("Введите отчество:");
            man.FatherName = Console.ReadLine();
            Console.WriteLine("Введите номер телефона без +:");
            string n = Console.ReadLine();
            long res;
            while(false == long.TryParse(n, out res))
            {
                Console.WriteLine("Поле пустое или введено неверно, попробуйте еще раз:\n");
                n = Console.ReadLine();
            }
            man.PhoneNumber = res;
            Console.WriteLine("Введите страну:");
            string countryTest = Console.ReadLine().Trim();
            while (countryTest == "")
            {
                Console.WriteLine("Это обязательное поле!");
                countryTest = Console.ReadLine();
            }
            man.Contry = countryTest;
            Console.WriteLine("Введите день рождения:");
            string day = Console.ReadLine();
            int dres;
            if (day == "")
            {
                man.DayOfBirdth = null;
            }
            else
            {
                while (false == int.TryParse(day, out dres) || int.Parse(day) < 0 || int.Parse(day) > 31)
                {
                    Console.WriteLine("Введите двузначное число от 1 до 31:\n");
                    day = Console.ReadLine();
                }
                man.DayOfBirdth = Convert.ToString(dres);
            }
            
            Console.WriteLine("Введите месяц рождения:");
            string month = Console.ReadLine();
            int mres;
            if (day == "")
            {
                man.MounthOfBirdth = null;
            }
            else
            {
                while (false == int.TryParse(month, out mres) || int.Parse(month) < 0 || int.Parse(month) > 12)
                {
                    Console.WriteLine("Введите двузначное число от 1 до 12:\n");
                    month = Console.ReadLine();
                }
                man.MounthOfBirdth = Convert.ToString(mres);
            }
            Console.WriteLine("Введите год рождения:");
            string year = Console.ReadLine();
            int yres;
            if (year == "")
            {
                man.YearOfBirdth = null;
            }
            else
            {
                while (false == int.TryParse(year, out yres) || int.Parse(year) < 0 || int.Parse(year) > 2019)
                {
                    Console.WriteLine("Введите правильный год:\n");
                    year = Console.ReadLine();
                }
                man.YearOfBirdth = Convert.ToString(yres);
            }
            Console.WriteLine("Введите Организацию:");
            man.Organisation = Console.ReadLine();
            Console.WriteLine("Введите Должность:");
            man.Position = Console.ReadLine();
            Console.WriteLine("Введите прочие заметки:");
            man.SomethingElse = Console.ReadLine();
            Memory.Add(man);
            Console.Clear();
            Console.WriteLine("Запись добавлена!");

        }

    }
}
