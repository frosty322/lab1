﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson1
{
    class Note
    {
        int IdCount = 0;
        public int ID { get; set;}
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string FatherName { get; set; }
        public long PhoneNumber { get; set; }
        public string Contry { get; set; }
        public string DayOfBirdth { get; set; }
        public string MounthOfBirdth { get; set; }
        public string YearOfBirdth { get; set; }
        public DateTime DateOfBirdth { get; set; } 
        public string Organisation { get; set; }
        public string Position { get; set; }
        public string SomethingElse { get; set; }
        public string ResultDate { get; set; }
        public Note()
        {
            IdCount++;
            ID = IdCount;
        }

        public override string ToString()
        {
            if (DayOfBirdth == null && MounthOfBirdth == null && YearOfBirdth == null)
            {
                ResultDate = "Данных нет";
            }
            else
            {
                DateOfBirdth = new DateTime(int.Parse(YearOfBirdth), int.Parse(MounthOfBirdth), int.Parse(DayOfBirdth));
                ResultDate = DateOfBirdth.ToShortDateString();
            }
            
            if (FatherName == "")
                FatherName = "Данных нет";
            if (Organisation == "")
                Organisation = "Данных нет";
            if (Position == "")
                Position = "Данных нет";
            if (SomethingElse == "")
                SomethingElse = "Данных нет";
            return $"Имя:{Name}\n" + $"Фамилия:{SecondName}\n" + $"Номер:{PhoneNumber}\n";


        }
        
    }
}
